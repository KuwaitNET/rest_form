from django import template
from rest_form.models.entry import FormEntryModel
from rest_form.models.redirect import ConditionalRedirectModel

register = template.Library()


@register.assignment_tag
def get_previous_url(submitter, token, form):
    if form.is_root():
        return
    # Reverse check for conditional redirects, tries to find previous form
    redirects = ConditionalRedirectModel.objects.filter(redirect_to=form)
    if redirects:
        for redirect in redirects:
            try:
                form_entry = FormEntryModel.objects.get(
                    submitter=submitter,
                    token=token,
                    form=redirect.form
                )
            except FormEntryModel.DoesNotExist:
                pass
            else:
                redirect_to = redirect.redirect_to if redirect.force_redirect else \
                    redirect.check_fields_conditions(
                    form_entry.get_fields_to_dict()
                )
                if redirect_to == form:
                    return redirect.form.get_absolute_url()
    # Otherwise, returns either previous sibling or parent form
    prev = form.get_prev_sibling()
    if prev:
        return prev.get_absolute_url()
    else:
        prev = form.get_parent()
        if prev:
            return prev.get_absolute_url()
    return
