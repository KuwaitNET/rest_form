import django.dispatch

after_submit_signal = django.dispatch.Signal(providing_args=['entries',])