# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from decimal import Decimal

from rest_form.models.entry import FieldEntryModel
from six import with_metaclass

from django import forms
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator, MaxLengthValidator, MinLengthValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import timedelta, now

from adminsortable.models import Sortable

from rest_form import deferred
from rest_form.utils import get_class, snake_case, get_submitter
from rest_form.validators import validate_regex_expr
from rest_form import settings as form_settings


class FieldQuerySet(models.QuerySet):

    def required(self):
        return self.filter(required=True)

    def files(self):
        return self.filter(
            field_class__in=form_settings.FILE_CLASSES)

    def non_files(self):
        return self.exclude(
            field_class__in=form_settings.FILE_CLASSES)


class FieldManager(models.Manager):
    def get_queryset(self):
        return FieldQuerySet(self.model, using=self._db)

    def required(self):
        return self.required()

    def files(self):
        return self.files()

    def non_files(self):
        return self.non_files()


class AbstractFieldFormMixin(object):
    """
    This are all methods and attrs for FieldModel class which will provide
    all functionality to construct a django form instance.
    """

    def get_init_form_args(self, token, request, update, is_formset, form_model):
        args = {
            'required':
                False if self.field_class in form_settings.FILE_CLASSES
                else self.required,
            'label': self.label,
            'help_text': self.help_text
        }

        # Get value for autofill, if the same field were filled before
        if not is_formset:
            if self.field_class not in form_settings.FILE_CLASSES:
                field_entry = FieldEntryModel.objects.filter(
                    entry__token=token,
                    entry__submitter=get_submitter(request),
                    field=self
                ).last()

                if field_entry:
                    args.update(initial=field_entry.value)

            if update and self.field_class not in form_settings.FILE_CLASSES:
                try:
                    field_entry = FieldEntryModel.objects.get(entry__token=token,
                                                              entry__submitter=get_submitter(request),
                                                              field=self,
                                                              form=form_model)
                except FieldEntryModel.DoesNotExist:
                    pass
                else:
                    if self.field_class in form_settings.ChoiceWithOtherField:
                        value = field_entry.value.split('; ')
                    else:
                        value = field_entry.value
                    args.update(initial=value)

        if self.field_class == form_settings.FileField:
            args.update(widget=forms.ClearableFileInput(attrs={'initial_required': self.required}))


        # CharField, EmailField, RegexField
        if self.field_class in (form_settings.CharField,
                                form_settings.EmailField,
                                form_settings.RegexField):
            args.update(
                max_length=self.max_length,
                min_length=self.min_length
            )

            if self.field_class == form_settings.RegexField:
                args.update(regex=self.regex or '')

            if self.placeholder_text and self.widget:
                if self.widget == self.TEXTAREA:
                    args.update(widget=forms.Textarea(placeholder=self.placeholder_text))
                if self.widget == self.PASSWORD:
                    args.update(widget=forms.PasswordInput(placeholder=self.placeholder_text))
                else:
                    args.update(widget=forms.TextInput(placeholder=self.placeholder_text))

        # IntegerField
        if self.field_class == form_settings.IntegerField:
            args.update(
                max_value=self.max_value,
                min_value=self.min_value,
            )

            if self.placeholder_text:
                args.update(widget=forms.TextInput(placeholder=self.placeholder_text))

        # DecimalField
        if self.field_class == form_settings.DecimalField:
            args.update(
                max_value=self.max_value,
                min_value=self.min_value,
                max_digits=self.max_digits or 10,
                decimal_places=self.decimal_places or 2
            )
            if self.placeholder_text:
                args.update(widget=forms.TextInput(placeholder=self.placeholder_text))

        # DateField
        if self.field_class == form_settings.DateField:
            args.update(
                input_formats=settings.DATE_INPUT_FORMATS
            )
            validators = []

            if self.min_date:
                validators.append(
                    MaxValueValidator(
                        now().date() - timedelta(days=self.min_date),
                        message=self.min_date_message
                    )
                )
            if self.max_date:
                validators.append(
                    MinValueValidator(
                        now().date() + timedelta(days=self.max_date),
                        message=self.max_date_message
                    )
                )

            if validators:
                args.update(validators=validators)

            if self.placeholder_text:
                args.update(widget=forms.DateInput(placeholder=self.placeholder_text))

        # ChoiceField
        if self.field_class == form_settings.ChoiceField:
            print('choices', self.choices.values_list('id', 'value'))
            args.update(
                choices=tuple(self.choices.values_list('id', 'value'))
            )
            if self.widget == self.CHECKBOX:
                args.update(
                    widget=forms.CheckboxInput()
                )
            if self.widget == self.RADIO_BUTTON:
                args.update(
                    widget=forms.RadioSelect()
                )

        # MultipleChoiceField
        if self.field_class in form_settings.MultipleChoiceField:
            args.update(
                choices=tuple(self.choices.values_list('id', 'value'))
            )
            if self.widget == self.CHECKBOX_MULTIPLE:
                args.update(
                    widget=forms.CheckboxSelectMultiple()
                )

        if self.field_class in form_settings.ChoiceWithOtherField:
            args.update(
                choices=tuple(self.choices.values_list('id', 'value', 'trigger'))
            )

        return args

    def get_form_base(self):
        for base, fields in form_settings.FORM_BASES.items():
            if self.field_class in fields:
                return base
        return form_settings.form_base

    def instantiate_form(self, token, request, update, is_formset, form_model):
        field_class = self.get_form_base() + self.field_class
        return get_class(field_class)(**self.get_init_form_args(
            token, request, update, is_formset, form_model))


class AbstractFieldSerializerMixin(object):
    """
    This are all methods and attrs for FieldModel class which will provide
    all functionality to construct a DRF serializer. It is built to use
    reder_form templatetag.
    """
    def get_init_serializer_args(self):
        # set file fields required to False to allow async upload
        args = {
            'required':
                False if self.field_class in (form_settings.FileField,
                                              form_settings.ImageField)
                else self.required
        }

        if self.help_text:
            args.update(help_text=self.help_text)

        if self.label:
            args.update(label=self.label)

        if self.field_class in (form_settings.CharField,
                                form_settings.EmailField,
                                form_settings.RegexField):
            if self.max_length:
                args.update(max_length=self.max_length)
            if self.min_length:
                args.update(min_length=self.min_length)
        if self.field_class in (form_settings.IntegerField,
                                form_settings.DecimalField):
            if self.max_value:
                args.update(
                    max_value=Decimal(str(self.max_value))
                )
            if self.min_value:
                args.update(
                    min_value=Decimal(str(self.min_value))
                )
        if self.field_class == form_settings.DecimalField:
            args.update(
                max_digits=self.max_digits or 10,
                decimal_places=self.decimal_places or 2
            )
        if self.field_class == form_settings.DateField:
            args.update(
                input_formats=settings.DATE_INPUT_FORMATS
            )
            validators = []

            if self.min_date:
                validators.append(
                    MinValueValidator(
                        now().date() - timedelta(days=self.min_date),
                        message=self.min_date_message
                    )
                )
            if self.max_date:
                validators.append(
                    MaxValueValidator(
                        now().date() + timedelta(days=self.max_date),
                        message=self.max_date_message
                    )
                )

            if validators:
                args.update(validators=validators)

        if self.field_class == form_settings.RegexField:
            args.update(
                regex=self.regex or ''
            )
        if self.field_class in (form_settings.ChoiceField,
                                form_settings.MultipleChoiceField):
            args.update(
                choices=tuple(self.choices.values_list('id', 'value'))
            )

        # TODO: fix this method, either move to form or find another solution.
        # if self.field_class in (form_settings.FileField,
        #                         form_settings.ImageField):
        #     template_path = "{template_pack}/{file}".format(
        #         template_pack=self.form.form_template,
        #         file="file_input.html"
        #     )
        #     try:
        #         get_template(template_path)
        #     except TemplateDoesNotExist:
        #         pass
        #     else:
        #         args.update(style={'base_template': 'file_input.html'})

        if self.widget or self.placeholder_text:
            style = {
                'placeholder': self.placeholder_text,
                'input_type': self.widget
            }
            if self.widget == self.TEXTAREA:
                style.update(base_template='textarea.html')
                if self.rows is not None:
                    style.update(rows=self.rows)
            if self.widget == self.RADIO_BUTTON:
                style.update(base_template='radio.html')

            if self.widget == self.CHECKBOX_MULTIPLE:
                style.update(base_template='checkbox_multiple.html')

            if self.widget == self.CHECKBOX:
                style.update(base_template='checkbox.html')
            args.update(style=style)

        return args

    def instantiate_serializer(self):
        field_class = form_settings.serializer_base + self.field_class
        return get_class(field_class)(**self.get_init_serializer_args())


@python_2_unicode_compatible
class AbstractField(with_metaclass(deferred.ForeignKeyBuilder,
                                   AbstractFieldSerializerMixin,
                                   AbstractFieldFormMixin,
                                   models.Model)):
    """
    Field for built-in form
    """
    TEXT, TEXTAREA, RADIO_BUTTON, PASSWORD, HIDDEN, \
        CHECKBOX, CHECKBOX_MULTIPLE, OPTIONAL_CHOICE = 'text', 'textarea', 'radio', \
                                      'password', 'hidden', 'checkbox', \
                                      'checkbox_multiple', 'optional_choice'
    WIDGET_CHOICES = (
        (TEXT, _("Text")),
        (TEXTAREA, _("Textarea")),
        (RADIO_BUTTON, _("Radio button")),
        (PASSWORD, _("Password")),
        (CHECKBOX, _("Checkbox")),
        (CHECKBOX_MULTIPLE, _("Checkbox Multiple")),
        (OPTIONAL_CHOICE, _("Optional Choice"))
    )
    name = models.CharField(_("name"), max_length=64)
    field_class = models.CharField(
        _("Field class"),
        choices=form_settings.FIELD_CLASSES,
        max_length=100
    )
    widget = models.CharField(
        _("Widget"),
        choices=WIDGET_CHOICES,
        max_length=34,
        default='',
        blank=True
    )
    label = models.CharField(
        _("Label"),
        max_length=form_settings.LABEL_MAX_LENGTH,
        help_text=_("Field label")
    )
    help_text = models.CharField(
        _("Help text"),
        blank=True,
        max_length=form_settings.HELP_TEXT_MAX_LENGTH
    )
    rows = models.PositiveSmallIntegerField(
        _("Rows"),
        null=True,
        blank=True,
        help_text=_("Number of rows for textarea")
    )
    regex = models.CharField(
        _("Regular expression"),
        max_length=255,
        blank=True,
        null=True,
        validators=[validate_regex_expr]
    )
    placeholder_text = models.CharField(
        _("Placeholder Text"),
        default='',
        blank=True,
        max_length=100
    )
    initial = models.TextField(_("Initial value"), blank=True, null=True)
    required = models.BooleanField(_("Required"), default=True)
    max_length = models.IntegerField(_("Max. length"), blank=True, null=True)
    min_length = models.IntegerField(_("Min. length"), blank=True, null=True)
    max_value = models.FloatField(_("Max. value"), blank=True, null=True)
    min_value = models.FloatField(_("Min. value"), blank=True, null=True)
    max_date = models.PositiveSmallIntegerField(
        _("Max. date"),
        blank=True,
        null=True,
        help_text=_('This will define the max date limit counted in days. '
                    'This will be calculated from current date.')
    )
    max_date_message = models.CharField(
        _("Max date error message"),
        max_length=200,
        blank=True,
        null=True,
        help_text=_('This is the error message shown for max date validator.')
    )
    min_date = models.PositiveSmallIntegerField(
        _("Min. date"),
        blank=True,
        null=True,
        help_text=_('This will define the min date limit counted in days. '
                    'This will be calculated from current date.')
    )
    min_date_message = models.CharField(
        _("Min date error message"),
        max_length=200,
        blank=True,
        null=True,
        help_text=_('This is the error message shown for min date validator.')
    )
    max_digits = models.IntegerField(_("Max. digits"), blank=True, null=True)
    decimal_places = models.IntegerField(
        _("Decimal places"),
        blank=True,
        null=True
    )
    min_size = models.FloatField(
        _("Min. file size"),
        blank=True,
        null=True,
        help_text=_("Minimum size of the uploaded file in MB.")
    )
    max_size = models.FloatField(
        _("Max. file size"),
        blank=True,
        null=True,
        help_text=_("Maximum size of the uploaded file in MB."),
        default=2.50
    )
    allowed_file_types = models.TextField(
        _("Allowed file types"),
        default='',
        blank=True,
        help_text=_("Provide a separated comma list of file extensions. IE: "
                    "doc, docx, xls, xlsx, zip, pdf, odt, png, gif.")
    )
    send_success_email = models.BooleanField(
        default=False,
        help_text=_("If checked, an email will be sent "
                    "to an email address from value of this field.")
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = FieldManager()

    def __str__(self):
        return '{id} - [{field_class}] {name}'.format(
            id=self.pk, field_class=self.get_field_class_display(),
            name=self.name
        )

    def has_choices(self):
        return self.choices.all().exists()

    class Meta:
        verbose_name = _("Field")
        verbose_name_plural = _("Fields")
        abstract = True

    def save(self, *args, **kwargs):
        self.name = snake_case(self.name)
        super(AbstractField, self).save(*args, **kwargs)

FieldModel = deferred.MaterializedModel(AbstractField)


@python_2_unicode_compatible
class AbstractChoice(with_metaclass(deferred.ForeignKeyBuilder, Sortable)):
    field = deferred.ForeignKey(
        'AbstractField',
        verbose_name=_('field'),
        related_name='choices'
    )
    value = models.CharField(
        _('Value'),
        max_length=form_settings.LABEL_MAX_LENGTH
    )
    trigger = models.BooleanField(
        _('Trigger'),
        default=False
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    class Meta:
        verbose_name = _('Choice')
        verbose_name_plural = _('Choices')
        ordering = ('order',)
        abstract = True

    def __str__(self):
        return self.value

ChoiceModel = deferred.MaterializedModel(AbstractChoice)
