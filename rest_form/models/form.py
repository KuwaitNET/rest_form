# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from six import with_metaclass

from django.db.models import Q
from django.conf import settings
from django.core.exceptions import ValidationError, MultipleObjectsReturned
from django.core.urlresolvers import reverse
from django.core.validators import MaxValueValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from adminsortable.models import Sortable
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from treebeard.mp_tree import MP_Node

from rest_form import settings as form_settings
from rest_form import deferred
from rest_form.models.entry import FormEntryModel, FormSubmitModel
from rest_form.models.field import AbstractField
from rest_form.utils import get_submitter


class FormFieldQuerySet(models.QuerySet):

    def active(self):
        return self.filter(is_active=True)

    def files(self):
        return self.filter(field__field_class__in=form_settings.FILE_CLASSES)

    def non_files(self):
        return self.exclude(field__field_class__in=form_settings.FILE_CLASSES)


class FormFieldManager(models.Manager):

    def get_queryset(self):
        return FormFieldQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def files(self):
        return self.get_queryset().files()

    def non_files(self):
        return self.get_queryset().non_files()


class AbstractFormField(with_metaclass(deferred.ForeignKeyBuilder,
                                       Sortable)):
    form = deferred.ForeignKey(
        'AbstractForm',
        verbose_name=_('form'),
        related_name='fields'
    )
    field = deferred.ForeignKey(
        AbstractField,
        verbose_name=_('field'),
    )
    is_active = models.BooleanField(_("Is active?"), default=True)

    objects = FormFieldManager()

    def __unicode__(self):
        return self.field.__unicode__()

    class Meta:
        verbose_name = _('Form Field')
        verbose_name_plural = _('Form Fields')
        ordering = ('order',)
        abstract = True

FormFieldModel = deferred.MaterializedModel(AbstractFormField)


class FormQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def public(self):
        return self.filter(login_required=False)

    def auth(self):
        return self.filter(Q(login_required=True) |
                           Q(guest_required=False))


class FormManager(models.Manager):
    def get_queryset(self):
        return FormQuerySet(self.model, using=self._db)

    def active(self):
        return self.get_queryset().active()

    def public(self):
        return self.active().public()

    def auth(self):
        return self.active().auth()

    def get_for_submitter(self, submitter):
        """
        :param submitter: settings.AUTH_USER_MODEL instance
        :return: forms which has the entries submitted by submitter
        """
        return self.filter()

    def get_for_request(self, request):
        if request.user.is_authenticated():
            return self.auth()
        return self.public()

    def get_from_path(self, path):
        try:
            return self.get(url_path=path)
        except self.model.DoesNotExist:
            return None


@python_2_unicode_compatible
class AbstractForm(with_metaclass(deferred.ForeignKeyBuilder, MP_Node)):
    """
    A form can have fields or no, or it can be used for
    navigational purposes.

    We can have the following cases:

    1. One form with fields
        - the submit method will called from the form directly

    2. One parent form and one or multiple children forms
        - moderated has to be False on all children
    """
    multiple = models.BooleanField(
        _('multiple entries'),
        default=False,
        help_text=_('if checked, form can be submitted multilpe times by the same user')
    )
    is_active = models.BooleanField(
        _("is active?"),
        default=True,
        db_index=True
    )
    login_required = models.BooleanField(
        _("login required?"),
        default=False,
        db_index=True,
        help_text=_("If checked, only registered users can view and fill "
                    "the form.")
    )
    guest_required = models.BooleanField(
        _("guest required?"),
        default=False,
        db_index=True,
        help_text=_("If checked, only guests users can view and fill "
                    "the form.")
    )
    name = models.CharField(_("name"), max_length=50)
    slug = AutoSlugField(
        _("slug"),
        max_length=50,
        unique=True,
        populate_from='name',
        editable=form_settings.EDITABLE_SLUG
    )
    description = RichTextField(
        _("description"),
        blank=True,
        default='',
        help_text=_("Describe for what is being used this form.")
    )
    success_message = models.TextField(
        _("success message"),
        blank=True,
        default='',
        help_text=_("Message showed to the user after submit.")
    )
    moderated = models.BooleanField(
        _("moderated"),
        default=False,
        help_text=_("Designates if the form entries requires moderation.")
    )
    # TODO: replace this with fk to Group model?
    notify_users = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("notify Users"),
        blank=True,
        related_name='+',
        help_text=_("Users which shall be notified on form entry submit. "
                    "Leave empty for if notifying is not required.")
    )
    send_email = models.BooleanField(
        _("send email"),
        default=False,
        help_text=_("If checked, the person entering the form will be sent "
                    "an email")
    )
    email_field = deferred.OneToOneField(
        'AbstractField',
        verbose_name=_("email field"),
        null=True,
        blank=True,
        related_name='+',
        help_text=_("Required only for guest users. Email field where to "
                    "notify the submitter is guest")
    )
    email_template = models.ForeignKey(
        'post_office.EmailTemplate',
        verbose_name=_("email template"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("Email template used to notify user on form submit. "
                    "Required only if send email is checked.")
    )
    url_path = models.CharField(
        _("URL path"),
        help_text=_("Full URL path"),
        max_length=1000,
        db_index=True,
        blank=True,
        default=''
    )
    page_template = models.CharField(
        _('page template'),
        help_text=_('Select page template which will be used to render form.'),
        max_length=200
    )
    success_page_template = models.CharField(
        _('success page template'),
        help_text=_('Select page template for default success page.'),
        max_length=200
    )
    form_template = models.CharField(
        _('form template'),
        help_text=_('Select form template which will be used to render form.'),
        max_length=200
    )
    # is_inline_formset = models.BooleanField(
    #     _('inline formset'),
    #     default=False,
    #     help_text=_('This form will become a parent form for a set of '
    #                 'formsets. <br><b>NOTE: Children\'s of this form will '
    #                 'not be considered steps anymore and will be formset for '
    #                 'the current form. </b><br>If you wish to have a next '
    #                 'step after an inline formset add the form as a sibling '
    #                 'after the inline formset.')
    # )
    is_formset = models.BooleanField(
        _('is formset'),
        default=False,
        help_text=_('Useful to create and handling a set of forms on the '
                    'same page.')
    )
    formset_can_delete = models.BooleanField(
        _('can delete'),
        default=True,
        help_text=_('Define if users are allowed to select forms for '
                    'deletion.')
    )
    formset_can_order = models.BooleanField(
        _('can order'),
        default=False,
        help_text=_('Define if users will be able to change order of forms.')
    )
    formset_max_num = models.PositiveSmallIntegerField(
        _('max number of forms'),
        default=10,
        help_text=_('Limit the form number user can add and submit.'),
        validators=[MaxValueValidator(1000)]
    )
    formset_min_num = models.PositiveSmallIntegerField(
        _('min number of forms'),
        default=0,
        help_text=_('Limit the form number user can add and submit.')
    )

    permission = models.ManyToManyField(
      settings.AUTH_USER_MODEL,
      verbose_name=_('Permission Users'),
      related_name='permitted_users',
      blank=True
    )

    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = FormManager()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Form")
        verbose_name_plural = _("Forms")
        ordering = ['path']
        abstract = True

    def clean(self):
        super(AbstractForm, self).clean()
        if self.send_email and self.email_template is None:
            raise ValidationError(
                _("If send email is checked an email template must be "
                  "provided!")
            )

        # if self.is_inline_formset and self.is_formset:
        #     raise ValidationError(
        #         _("A form can't be a formset and an inline formset in the "
        #           "same time")
        #     )

    def get_fields(self):
        return self.fields.active()

    def get_path(self):
        return '/'.join([form.slug for form in self.get_ancestors()] +
                        [self.slug])

    def is_wizard(self):
        """
        Define if form has multiple steps.
        :returns: bool
        """
        return self.get_children().exists()

    def has_formset_childrens(self):
        """
        Define if form has multiple steps.
        :returns: bool
        """
        return self.get_children().filter(is_formset=True).exists()

    def is_single(self):
        """
        Define if form has a single step.
        :return: bool
        """
        return not self.is_wizard() and not self.get_ancestors().exists()

    def get_next(self, submitter, token):
        """
        Navigates through tree nodes and checks conditionals before retrieving
        next from.

        NOTE
        It uses the get_hierarchical_next() method to navigate through nodes
        which is limited to only 4 children's.

        How it works:
            1) If redirect_to defined, use it
            2) If there is no FormEntry for current user/token return next
            step from hierarchy.
            3) If FormEntry exist, check conditionals and if they are met
            go to the step returned by condition.
            4) If no conditions are met go to next step from hierarchy.

        """
        try:
            form_entry = FormEntryModel.objects.get(
                Q(submitter=submitter) &
                Q(token=token) &
                Q(form=self)
            )
        except MultipleObjectsReturned:
            form_entry = FormEntryModel.objects.filter(
                Q(submitter=submitter) &
                Q(token=token) &
                Q(form=self)
            ).latest('id')
        except FormEntryModel.DoesNotExist:
            next_form = self.get_hierarchical_next(submitter, token)
            if next_form:
                return next_form
            return
        else:
            for conditional in self.conditional_redirects.all():
                next_form = conditional.check_fields_conditions(
                    form_entry.get_fields_to_dict()
                )
                if next_form:
                    return next_form
            childrens = self.get_descendants().active().exclude(is_formset=True).count()

            if self.is_root() or childrens:
                next_form = self.get_descendants().first()
                return next_form
            # next_form = self.get_next_sibling()
            next_form = self.get_hierarchical_next(submitter, token)
            next_form_copy = next_form
            if next_form:
                all_form = next_form.get_siblings()
                for form in all_form:
                    if form == next_form_copy:
                        return form
                    try:
                        form_entry = FormEntryModel.objects.get(
                            Q(submitter=submitter) &
                            Q(token=token) &
                            Q(form=form)
                        )
                        for conditional in form.conditional_redirects.all():
                            next_form = conditional.check_fields_conditions(
                                form_entry.get_fields_to_dict()
                            )
                            if next_form:
                                try:
                                    form_entry = FormEntryModel.objects.get(
                                        Q(submitter=submitter) &
                                        Q(token=token) &
                                        Q(form=next_form)
                                    )
                                except FormEntryModel.DoesNotExist:
                                    if next_form:
                                        return next_form
                    except FormEntryModel.DoesNotExist:
                        return form

            return next_form

    def exclude_form_list(self,submitter,token):
        exclude_form_list = []
        all_form = self.get_root().get_descendants()
        for form in all_form:
            try:
                form_entry = FormEntryModel.objects.get(
                    Q(submitter=submitter) &
                    Q(token=token) &
                    Q(form=form)
                )
            except MultipleObjectsReturned:
                pass
            except FormEntryModel.DoesNotExist:
                pass
            else:
                for conditional in form.conditional_redirects.all():
                    next_form = conditional.check_fields_conditions(
                        form_entry.get_fields_to_dict()
                    )
                    if not next_form:
                        for inner_form in form.get_descendants():
                            if inner_form.id == conditional.redirect_to.id:
                                exclude_form_list.append(inner_form)

        return exclude_form_list

    def get_hierarchical_next(self, submitter, token):
        """
        Navigates through tree nodes only. It does not care about
        form conditionals.


            1) If form is has children's return first active child.
            2) Else if form is not root of tree and has sibling return
        first sibling.
            3) Navigates back up on tree to the root by checking siblings of
        parent.

        ie.
                                                GrandChildX
                                              /
                                GrandChildOne
                              /
                    ChildOne  - GrandChildTwo - GrandChildY
                   /          \
                  /             GrandChildThree
                 /
                /               GrandChildFor
               /              /
        Parent ---- ChildTwo
               \              \ GrandChildFive
                \
                 \
                    ChildThree
        """

        def _get_branch_sibling(node):
            if not node.is_root():
                form_entries_pks = FormEntryModel.objects.filter(
                    Q(submitter=submitter) &
                    Q(token=token)
                ).values_list('form_id', flat=True)

                sibling = node.get_siblings().active().exclude(is_formset=True)
                sibling = sibling.exclude(path=node.path)
                sibling = sibling.exclude(pk__in=form_entries_pks)
                sibling = sibling.first()

                if sibling:
                    if sibling.login_required and not submitter.is_authenticated():
                        return None
                    return sibling
                else:
                    return _get_branch_sibling(node.get_parent())

        for form in self.get_descendants().active().exclude(is_formset=True):
            if form.login_required and not submitter.is_authenticated():
                return
            return form

        return _get_branch_sibling(self)

    def get_previous(self, submitter, token):
        """
        Get previous step of the current form.

            1. If form is root return None.
            2. If form has FormEntry retrieve previous by filtering using
        current form if it doesn't return last form entry.
        :return: FormObj
        """
        if self.is_root():
            return

        qs = FormEntryModel.objects.filter(submitter=submitter, token=token)
        qs = qs.order_by('pk')

        if qs.exists():
            try:
                form_entry = qs.get(form=self)
            except FormEntryModel.DoesNotExist:
                return qs.last().form
            else:
                return qs.filter(pk__lt=form_entry.pk).last().form

    def count_required_steps(self):
        """
        Count total required steps.
        :return: int
        """
        return self.get_root().get_descendants().active().count()

    def get_forms_entires(self, request, token):
        return FormEntryModel.objects.filter(submitter=get_submitter(request), token=token)

    def calculate_progress(self,request, forms, token):
        """
            Calculate the overall progress of forms
            :return float
        """
        user_entries = self.get_forms_entires(request, token)
        percentage_progress = None
        if forms:
            percentage_progress = round(len(user_entries) * 100 / len(forms))
        return percentage_progress


    def has_fields(self):
        """
        Define if form has available fields.
        :return: bool
        """
        return self.fields.active().exists()

    def has_required_files(self):
        return self.fields.active().filter(
            required=True,
            field_class__in=form_settings.FIELD_CLASSES).exists()

    def has_previous(self):
        return not self.is_root()

    def get_absolute_url(self):
        return reverse('rest_form:create', args=(self.url_path,))

    def is_last(self, submitter, token):
        return not bool(self.get_next(submitter=submitter, token=token))

    def get_definition_url(self):
        return reverse('rest_form:api:definition-detail', args=(self.pk,))

    def get_previous_url(self, submitter, token):
        previous_step = self.get_previous(submitter=submitter, token=token)
        if previous_step:
            return previous_step.get_absolute_url()

    def get_redirect_url(self, request, submitter, token, entry_pks=None, custom_next_url=None):
        """
        Rules:
            1) If custom next url then redirect to Custom Next Url
            2) If Check if the form (Form model Instance) is wizard redirect
        to the next form in the wizard.
            3) Else redirect to success page.
            4) If it is last from then create entry in form submit
        """
        next_url = None
        next_form = self.get_next(submitter=submitter, token=token)

        if next_form:
            next_url = next_form.get_absolute_url()

        if custom_next_url:
            next_url = custom_next_url

        # if it is last form(Which doesn't have next form) We need to create entry in form submit
        if not next_form:
            try:
                root_form = FormEntryModel.objects.get(submitter=submitter,
                                                       token=token,
                                                       form__depth=1)
            except FormEntryModel.DoesNotExist:
                pass
            else:
                try:
                    del request.session['{}-token'.format(root_form.form.name)]
                except KeyError:
                    pass
            if FormEntryModel.objects.filter(token=token, submitter=submitter, form_submit__isnull=True).exists():
                form_submit = FormSubmitModel.objects.create(submitted=True)
                FormEntryModel.objects.filter(token=token, submitter=submitter, form_submit__isnull=True).update(
                    form_submit=form_submit)
                form_submit.after_submit()
        if next_url:
            return next_url
        return reverse('rest_form:success', args=(self.url_path,))
    

    def save(self, *args, **kwargs):
        super(AbstractForm, self).save(*args, **kwargs)
        # TODO: validate the parent form is not a formset if current is formset
        if self.url_path != self.get_path():
            self.url_path = self.get_path()
            kwargs['force_insert'] = False
            super(AbstractForm, self).save(*args, **kwargs)


FormModel = deferred.MaterializedModel(AbstractForm)