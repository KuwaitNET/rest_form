from __future__ import unicode_literals

import ast

from django.core.exceptions import ObjectDoesNotExist
from six import with_metaclass

from django.core.urlresolvers import reverse
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, get_language_from_request
from django.utils.timezone import now
from django.utils.functional import cached_property

from ipware.ip import get_ip
from jsonfield.fields import JSONField

from rest_form import settings as form_settings
from rest_form import deferred
from rest_form.utils import get_upload_path
from rest_form.signals import after_submit_signal


class FieldEntryQuerySet(models.QuerySet):

    def fields(self):
        return self.exclude(field__field_class__in=form_settings.FILE_CLASSES)

    def files(self):
        return self.filter(field__field_class__in=form_settings.FILE_CLASSES)

    def required(self):
        return self.filter(field__required=True)


class FormEntryManager(models.Manager):
    def create_for_form(self, form, token, submitter, request):
        return self.create(
            form=form,
            submitter=submitter,
            token=token,
            stored_request=self.stored_request(request)
        )

    def stored_request(self, request):
        """
        Extract useful information about the request to be used for emulating
        a Django request during offline rendering.
        """
        return {
            'language': get_language_from_request(request),
            'absolute_base_uri': request.build_absolute_uri('/'),
            'remote_ip': get_ip(request),
            'user_agent': request.META.get('HTTP_USER_AGENT'),
        }


@python_2_unicode_compatible
class AbstractFormSubmit(with_metaclass(deferred.ForeignKeyBuilder,
                                        models.Model)):
    approved = models.NullBooleanField(
        _("Approved?"),
        help_text=_("Required if the parent form requires approval.")
    )
    under_moderation = models.BooleanField(
        _("Under moderation"),
        default=False,
        help_text=_('Checked if the form entry is under moderation.')
    )
    entry_time = models.DateTimeField(_("Date/time"), null=True, blank=True)
    submitted = models.BooleanField(_('Submitted'), default=False)
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    def __str__(self):
        return '[{}] - {}'.format(
            self.form,
            self.submitter
        )

    @cached_property
    def form(self):
        for entry in self.entries.all():
            if entry.form.is_root():
                return entry.form

    @cached_property
    def submitter(self):
        form_entry = self.entries.first()
        if form_entry is not None:
            return form_entry.get_submitter()

    def get_export_to_pdf_url(self):
        return reverse('rest_form:submit_to_pdf', args=(self.pk,))

    def get_download_attachments_url(self):
        return reverse('rest_form:download_attachments', args=(self.pk,))

    class Meta:
        abstract = True

    def reject(self):
        self.submitted = False
        self.save()

    def after_submit(self):
        after_submit_signal.send(None, submit=self)

    def submit(self, request, additional_ctx=None):
        if self.submitted:
            return
        email_template = 'form_submission_admin'
        entry = self.entries.latest('created')
        form = entry.form
        ctx = {'form': form, 'entry': self}
        if additional_ctx is not None:
            ctx.update(additional_ctx)
        emails = list()

        # # send mails

        # update object
        self.entry_time = now()
        self.submitted = True
        self.save()

FormSubmitModel = deferred.MaterializedModel(AbstractFormSubmit)


@python_2_unicode_compatible
class AbstractFormEntry(with_metaclass(deferred.ForeignKeyBuilder,
                                       models.Model)):
    """
    An entry submitted via a user-built form.

    * Submit method will be called when all forms are completed.
    """
    form = deferred.ForeignKey(
        'AbstractForm',
        related_name="entries",
        on_delete=models.SET_NULL,
        null=True
    )
    parent = models.ForeignKey(
        'self',
        verbose_name=_('Parent form'),
        blank=True,
        null=True
    )
    form_submit = deferred.ForeignKey(
        'AbstractFormSubmit',
        null=True,
        blank=True,
        related_name='entries',
        help_text=_("Form submit object is being created when all entries "
                    "needed for the form to be completed are created.")
    )
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form")
    )
    token = models.UUIDField(
        _("Token"),
        null=True,
        blank=True,
        editable=False,
        help_text=_("Token unique for a set of form entries or for a single"
                    "instance of a form. Required for identity purpose.")
    )
    stored_request = JSONField(
        default={},
        help_text=_("Parts of the Request objects on the moment of "
                    "submission.")
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)

    objects = FormEntryManager()

    def __str__(self):
        if self.form_submit:
            return 'Entry for [{}] - [{}]'.format(self.form, self.form_submit.pk)
        return 'Entry for [{}]'.format(self.form)

    def get_submitter(self):
        return self.submitter or self.token

    class Meta:
        verbose_name = _("Form entry")
        verbose_name_plural = _("Form entries")
        abstract = True
        ordering = ['form__path']

    @cached_property
    def as_form_instance(self):
        pass

    @cached_property
    def as_serializer_instance(self):
        # TODO: change to ORM only
        instances = self.field_entries.values_list('field__name', 'value')
        return {field_name: value for field_name, value in instances}

    @cached_property
    def all_entries(self):
        """
        :return: Queryset with all entries related to the form/wizard
        """
        qs = FormEntryModel.objects.filter(form_submit=self.form_submit)
        return qs.filter(Q(submitter=self.submitter) & Q(token=self.token))

    def can_be_submitted(self):
        return self.all_entries.count() == self.form.count_required_steps() + 1

    def get_fields_to_dict(self):
        """
        Returns all fields(name/value) pairs into a dictionary.
        Does not return file values
        :return: dict
        """
        fields = self.field_entries.filter(file='')
        return dict(fields.values_list('field__name', 'value'))

FormEntryModel = deferred.MaterializedModel(AbstractFormEntry)


@python_2_unicode_compatible
class AbstractFieldEntry(with_metaclass(deferred.ForeignKeyBuilder,
                                        models.Model)):
    """
    A single field value for a form entry submitted via a user-built form.
    """
    form = deferred.ForeignKey(
        'AbstractForm',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='field_entries'
    )
    entry = deferred.ForeignKey(
        'AbstractFormEntry',
        null=True,
        blank=True,
        related_name='field_entries'
    )
    formset_order = models.PositiveSmallIntegerField(
        _('Formset order number'),
        help_text=_("used internally, don't touch"),
        blank=True,
        null=True
    )
    formset_id = models.CharField(
        _('Formset ID'),
        help_text=_("used internally, don't touch"),
        null=True,
        blank=True,
        max_length=120
    )
    # TODO: store the field name for history if field gets deleted
    field_name = models.CharField(
        _('Field name'),
        max_length=200,
        blank=True,
        default=''
    )
    field = deferred.ForeignKey(
        'AbstractField',
        null=True,
        on_delete=models.SET_NULL,
        related_name='entries'
    )
    value = models.CharField(
        max_length=form_settings.FIELD_MAX_LENGTH,
        blank=True,
        default=''
    )
    file = models.FileField(
        _("File"),
        upload_to=get_upload_path,
        null=True,
        blank=True
    )
    submitter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Submitter"),
        null=True,
        blank=True,
        help_text=_("User who submitted the form")
    )
    token = models.UUIDField(
        _("Token"),
        null=True,
        blank=True,
        editable=False,
        help_text=_("Token unique for a set of form entries or for a single"
                    "instance of a form. Required for identity purpose.")
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    modified = models.DateTimeField(_("Modified"), auto_now=True)
    objects = FieldEntryQuerySet.as_manager()

    def __str__(self):
        return '{} - {}'.format(self.field.__str__(), self.value)

    class Meta:
        abstract = True
        verbose_name = _("Field entry")
        verbose_name_plural = _("Field entries")

    def get_value(self):
        if self.field.has_choices():
            if ';' in self.value:
                choice_id, text = self.value.split('; ')
                try:
                    return b"%s %s" % (self.field.choices.get(pk=choice_id).value, text)
                except ObjectDoesNotExist:
                    return self.value
            if not self.value:
                return
            choices = ast.literal_eval(self.value)
            try:
                values = self.field.choices.filter(pk__in=choices).values_list('value', flat=True)
                return ', '.join([b'%s' % value for value in values])
            except TypeError:
                return self.field.choices.get(pk=choices).value
        return self.value

FieldEntryModel = deferred.MaterializedModel(AbstractFieldEntry)
