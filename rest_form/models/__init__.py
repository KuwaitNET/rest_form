# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from rest_form.settings import FORK

FORK = False

if not FORK:
    from rest_form.models.form import AbstractForm, AbstractFormField
    from rest_form.models.field import AbstractField, AbstractChoice
    from rest_form.models.entry import AbstractFieldEntry, AbstractFormEntry, AbstractFormSubmit
    from rest_form.models.redirect import AbstractCondition, AbstractConditionalRedirect

    class Form(AbstractForm):
        class Meta:
            verbose_name = _("Form")
            verbose_name_plural = _("Forms")
            ordering = ['path', '-modified']

    class FormField(AbstractFormField):
        class Meta:
            verbose_name = _('field')
            verbose_name_plural = _('fields')
            ordering = ['order']


    class Field(AbstractField):
        class Meta:
            verbose_name = _('Field')
            verbose_name_plural = _("Fields")


    class Choice(AbstractChoice):
        class Meta:
            verbose_name = _('Choice')
            verbose_name_plural = _("Choices")
            ordering = ('order',)


    class FieldEntry(AbstractFieldEntry):
        class Meta:
            verbose_name = _("Field entry")
            verbose_name_plural = _("Field entries")
            ordering = ('-created', 'modified')


    class FormEntry(AbstractFormEntry):
        class Meta:
            verbose_name = _("Form entry")
            verbose_name_plural = _("Form entries")
            ordering = ('-created', 'modified')

    class FormSubmit(AbstractFormSubmit):
        class Meta:
            verbose_name = _("Form submit")
            verbose_name_plural = _("Form submits")
            ordering = ('-created', 'modified')

    class Condition(AbstractCondition):
        class Meta:
            verbose_name = _("condition")
            verbose_name_plural = _("conditions")

    class ConditionalRedirect(AbstractConditionalRedirect):
        class Meta:
            verbose_name = _("conditional redirect")
            verbose_name_plural = _("conditional redirect")