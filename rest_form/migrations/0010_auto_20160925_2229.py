# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-09-25 22:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest_form', '0009_form_form_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='widget',
            field=models.CharField(blank=True, choices=[('text', 'Text'), ('textarea', 'Textarea'), ('radio', 'Radio button'), ('password', 'Password'), ('checkbox', 'Checkbox'), ('checkbox_multiple', 'Checkbox Multiple')], default='', max_length=34, verbose_name='Widget'),
        ),
    ]
