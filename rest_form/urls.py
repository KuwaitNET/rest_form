from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import url, include
from django.contrib.admin.views.decorators import staff_member_required
from django.views.i18n import javascript_catalog

from rest_form import views
from rest_form.constants import FORM_PATH_REGEX


urlpatterns = [

    url(r'^api/', include('rest_form.api.urls', namespace='api')),
    url(
        r'^success/{}/$'.format(FORM_PATH_REGEX),
        views.SuccessURL.as_view(),
        name='success'
    ),

    # TODO: Update this view for non-auth users. Currently there is no
    # middleware to solve this auth user.
    url(
        r'^create/{}/$'.format(FORM_PATH_REGEX),
        views.FormModelView.as_view(),
        name='create'
    ),
    url(r'^jsi18n/$', javascript_catalog, name='javascript-catalog'),
    url(r'^submit/(?P<pk>\d+)/export/pdf/$', staff_member_required(views.SubmitToPDFView.as_view(), login_url=settings.LOGIN_URL), name='submit_to_pdf'),
    url(r'^export/(?P<pk>\d+)/download-attachments/zip/$', staff_member_required(views.DownloadSubmitAttachmentsView.as_view(), login_url=settings.LOGIN_URL),
        name='download_attachments'),
]