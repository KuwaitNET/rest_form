# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import unittest

from django.test import Client

from rest_form.models.form import FormModel


class ViewTest(unittest.TestCase):
    def setUp(self):
        self.form = FormModel.objects.create()
        self.client = Client()

    def test_create_view(self):

        response = self.client.get(self.form.get_absolute_url())

        self.assertEqual(response.status_code, 200)
