# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import uuid
import re

import django
from django.utils.module_loading import import_string
from django.utils.functional import allow_lazy
from django.utils.text import force_text
from django.utils.safestring import mark_safe

from rest_form import settings as form_settings


def unique_upload(instance, filename):
    ext = filename.split('.').pop()
    return "{}.{}".format(uuid.uuid4(), ext)


def get_upload_path(instance, filename):
    return import_string(form_settings.UPLOAD_FUNCTION)(instance, filename)


def get_class(class_path):
    """
    dynamically get a class from a string
    """
    class_data = class_path.split(".")
    module_path = ".".join(class_data[:-1])
    class_str = class_data[-1]

    module = import_string(module_path)
    # Finally, we retrieve the Class
    return getattr(module, class_str)


def snake_case(value):
    """
    Converts spaces to underscore. Removes characters that
    aren't alphanumerics, underscores, or hyphens. Converts to lowercase.
    Also strips leading and trailing whitespace.
    """
    value = force_text(value)
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return mark_safe(re.sub('[-\s]+', '_', value))
snake_case = allow_lazy(snake_case)


def user_is_authenticated(user):
    if django.VERSION >= (1, 10):
        return user.is_authenticated
    else:
        return user.is_authenticated()

def get_submitter(request):
    submitter = request.user
    if not user_is_authenticated(request.user):
        submitter = None
    return submitter
