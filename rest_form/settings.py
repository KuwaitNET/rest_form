# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.utils.translation import ugettext_lazy as _


APP_LABEL = getattr(settings, 'REST_FORM_APP_LABEL', 'rest_form')

serializer_base = 'rest_framework.serializers.'
form_base = 'django.forms.fields.'

CharField = getattr(settings, 'REST_FORMS_CharField', 'CharField')
EmailField = getattr(settings, 'REST_FORMS_EmailField', 'EmailField')
URLField = getattr(settings, 'REST_FORMS_URLField', 'URLField')
IntegerField = getattr(settings, 'REST_FORMS_IntegerField', 'IntegerField')
DecimalField = getattr(settings, 'REST_FORMS_DecimalField', 'DecimalField')
BooleanField = getattr(settings, 'REST_FORMS_BooleanField', 'BooleanField')
DateField = getattr(settings, 'REST_FORMS_DateField', 'DateField')
DateTimeField = getattr(settings, 'REST_FORMS_DateTimeField', 'DateTimeField')
TimeField = getattr(settings, 'REST_FORMS_TimeField', 'TimeField')
ChoiceField = getattr(settings, 'REST_FORMS_ChoiceField', 'ChoiceField')
MultipleChoiceField = getattr(settings, 'REST_FORMS_MultipleChoiceField',
                              'MultipleChoiceField')
RegexField = getattr(settings, 'REST_FORMS_RegexField', 'RegexField')
FileField = getattr(settings, 'REST_FORMS_FileField', 'FileField')
ImageField = getattr(settings, 'REST_FORMS_ImageField', 'ImageField')
ChoiceWithOtherField = getattr(settings, 'REST_FORMS_ChoiceWithOtherField', 'ChoiceWithOtherField')
KWCivilIDNumberField = getattr(settings, 'REST_FORMS_KWCivilIDNumberField', 'KWCivilIDNumberField')

FILE_CLASSES = (FileField, ImageField)


FIELD_CLASSES = (
    (CharField, _('Text')),
    (EmailField, _('Email')),
    (URLField, _('URL')),
    (IntegerField, _('Number')),
    (DecimalField, _('Decimal')),
    (BooleanField, _('Yes/No')),
    (DateField, _('Date')),
    (DateTimeField, _('Date & time')),
    (TimeField, _('Time')),
    (ChoiceField, _('Choice')),
    (MultipleChoiceField, _('Multiple choices')),
    (RegexField, _('Regex')),
    (FileField, _('File')),
    (ImageField, _('Image')),
    (ChoiceWithOtherField, _('Choice field with optional answer')),
    (KWCivilIDNumberField, _('Kuwait Civil ID'))
    # ('captcha.fields.CaptchaField', _('Captcha')),
)

FORM_BASES = {
    'django.forms.fields.': [CharField, EmailField, URLField, IntegerField,
                             DecimalField, BooleanField, DateField,
                             DateTimeField, TimeField, ChoiceField,
                             MultipleChoiceField, RegexField, FileField,
                             ImageField],
    'rest_form.forms.forms.': [ChoiceWithOtherField,],
    'localflavor.kw.forms.': [KWCivilIDNumberField,]
}

# The maximum allowed length for field values.
FIELD_MAX_LENGTH = getattr(settings, "REST_FORM_FIELD_MAX_LENGTH", 2000)

# The maximum allowed length for field labels.
LABEL_MAX_LENGTH = getattr(settings, "REST_FORM_LABEL_MAX_LENGTH", 2000)

# Upload function to handle uploaded files path
UPLOAD_FUNCTION = getattr(settings, "REST_FORM_UPLOAD_FUNCTION",
                          'rest_form.utils.unique_upload')

# Boolean controlling whether form slug is editable in the admin.
EDITABLE_SLUG = getattr(settings, "REST_FORM_EDITABLE_SLUG", True)

# Char to use as a field delimiter when exporting form responses as CSV.
CSV_DELIMITER = getattr(settings, "REST_FORM_CSV_DELIMITER", ",")

# The maximum allowed length for field help text
HELP_TEXT_MAX_LENGTH = getattr(settings, "REST_FORM_HELPTEXT_MAX_LENGTH", 100)

# The maximum allowed length for field choices
CHOICES_MAX_LENGTH = getattr(settings, "REST_FORM_CHOICES_MAX_LENGTH", 1000)

# Action URL, which is processing the form submission
ACTION_VIEW = getattr(settings, "REST_FORM_ACTION_VIEW", 'rest_form:api:submit')

# File upload URL used to upload files async during form completion
FILE_UPLOAD_VIEW = getattr(settings, "REST_FORM_UPLOAD_URL", 'rest_form:api:file_upload')

# Form options for form model instance
FORM_OPTIONS = getattr(settings, "REST_FORM_FORM_OPTIONS", {})

REST_FORM_FORM_TEMPLATES = (
    ('rest_framework/vertical', _('Bootstrap Vertical')),
    ('rest_framework/horizontal', _('Bootstrap Horizontal')),
    ('rest_framework/inline', _('Bootstrap Inline'))
)
if hasattr(settings, 'REST_FORM_FORM_TEMPLATES'):
    REST_FORM_FORM_TEMPLATES += getattr(settings, 'REST_FORM_FORM_TEMPLATES')

REST_FORM_PAGE_TEMPLATES = (
    ('rest_form/form.html', _('Default')),
)

REST_FORM_SUCCESS_PAGE_TEMPLATES = (
    ('rest_form/success.html', _('Default')),
)
if hasattr(settings, 'REST_FORM_SUCCESS_PAGE_TEMPLATES'):
    REST_FORM_SUCCESS_PAGE_TEMPLATES += getattr(
        settings,
        'REST_FORM_SUCCESS_PAGE_TEMPLATES'
    )

if hasattr(settings, 'REST_FORM_PAGE_TEMPLATES'):
    REST_FORM_PAGE_TEMPLATES += getattr(settings, 'REST_FORM_PAGE_TEMPLATES')

# Designated if form models are to be inherited in local app.
FORK = getattr(settings, "REST_FORM_FORK", False)

FIELD_ATTRS = (
    'help_text', 'max_length', 'min_length', 'max_value',
    'min_value', 'max_digits', 'decimal_places', 'min_size',
    'max_size'
)



# {'first': {
#     'serializer_class': None, # serializer path of FormEntrySerializer
#                               # replacement. It overrides the rest of the
#                               # keys
#     'validators': [], # path of the validate functions to be attached on
#                       # the serializer instance,
#                       # validate_field_name(self, value)
#     'actions': {
#         'create': 'rest_form.utils.create', # path to create function
#         'update': 'rest_form.utils.create', # Bulk update function
#         'post_create': None, # function to be called after create
#                              # method ends. args FormEntry instance
#         'post_update': None,
#     },
#     'submit': False, # ie. login form does not require a submit action.
#     'redirect_url': None,
#     }
# }
