from __future__ import unicode_literals

from django.apps import AppConfig


class RestFormConfig(AppConfig):
    name = 'rest_form'
