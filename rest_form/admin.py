# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from rest_form import settings as form_settings
from rest_form.models.form import FormModel, FormFieldModel
from rest_form.models.field import FieldModel, ChoiceModel
from rest_form.models.entry import (FormSubmitModel, FormEntryModel,
                                    FieldEntryModel)
from rest_form.models.redirect import ConditionModel, ConditionalRedirectModel


ENTRY_TIME_FIELDS = (_("Entry time"), {
    'fields': (('created', 'modified'),),

})


class ChoiceInlines(admin.TabularInline):
    model = ChoiceModel
    extra = 0

    class Media:
        js = (
            "/static/js/jquery-1.11.3.js",
            "/static/js/field.js"
        )


class FieldAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'label', 'field_class', 'widget')
    list_filter = ('field_class', 'widget',)
    list_editable = ('label', 'field_class', 'widget')
    readonly_fields = ('created', 'modified')
    fieldsets = (
        (None, {
            'fields': ('name',),

        }),
        (_("Settings"), {
            'fields': ('required', 'label', 'field_class', 'widget',
                       'help_text', 'placeholder_text', 'send_success_email'),
            'classes': ('collapse',),
        }),
        (_("Attributes"), {
            'fields': (
                'regex', 'initial', 'max_length', 'min_length',
                'max_value', 'min_value', 'max_digits', 'max_date',
                'max_date_message', 'min_date', 'min_date_message',
                'decimal_places', 'min_size', 'max_size', 'allowed_file_types'
            ),
            'classes': ('collapse',),
        }),
        ENTRY_TIME_FIELDS
    )
    inlines = [ChoiceInlines]
    save_as = True


class FormFieldInline(SortableTabularInline):
    model = FormFieldModel
    extra = 1
    fields = ('field', 'is_active')


class FormFieldAdmin(admin.ModelAdmin):
    fields = ('form', 'field', 'is_active')
    list_display = ('__unicode__', 'form',)


class FormAdmin(NonSortableParentAdmin, TreeAdmin):
    form = movenodeform_factory(
        FormModel,
        widgets={
            'page_template': forms.Select(
                choices=form_settings.REST_FORM_PAGE_TEMPLATES
            ),
            'success_page_template': forms.Select(
                choices=form_settings.REST_FORM_SUCCESS_PAGE_TEMPLATES
            ),
            'form_template': forms.Select(
                choices=form_settings.REST_FORM_FORM_TEMPLATES
            )
        }
    )
    list_display = ('name', 'is_active')
    list_editable = ('is_active',)
    prepopulated_fields = {"slug": ("name",)}
    readonly_fields = ('created', 'modified')
    save_as = True
    filter_horizontal = ('notify_users', 'permission')
    # inlines = [FormFieldInline]

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'permission':
            kwargs['queryset'] = User.objects.filter(is_staff=True, is_superuser=False)
        return super(FormAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    def get_prepopulated_fields(self, request, obj=None):
        if form_settings.EDITABLE_SLUG:
            return {"slug": ("name",)}

    fieldsets = (
        (None, {
            'fields': (
                'is_active', ('name', 'slug'),
                ('login_required', 'guest_required'), 'description',
                'success_message'
            )
        }),
        (_('Formset'), {
            'fields': ('is_formset', 'formset_can_delete',
                       'formset_can_order', 'formset_max_num',
                       'formset_min_num',)
        }),
        (_("Email settings"), {
            'fields': (
                'notify_users', 'send_email', ('email_template', 'email_field')
            )
        }),

        (_("permission"), {
            'fields': (
               'permission',
            )
        }),

        (_("Settings"), {
            'fields': (
                'moderated', 'multiple', 'page_template',
                'success_page_template', 'form_template'
            )
        }),
        (_("Tree settings"), {
           'fields': ('_position', '_ref_node_id')
        }),
        ENTRY_TIME_FIELDS
    )


class FieldEntryInline(admin.TabularInline):
    """
    Inlines for form entries
    """
    model = FieldEntryModel
    fields = ('field', 'value', 'file')
    extra = 0

    def has_add_permission(self, request):
        return False


class FieldEntryAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'submitter', 'token', 'form')


class FormEntryAdmin(admin.ModelAdmin):
    """
    Admin for form entries
    """
    list_display = ('__unicode__', 'submitter', 'form')
    readonly_fields = ('created', 'modified', 'token',)
    inlines = [FieldEntryInline]
    fieldsets = (
        (None, {
            'fields': (('form', 'parent', 'form_submit'),)
        }),
        (_("Submitter"), {
            'fields': (('submitter', 'token'),)
        }),
        ENTRY_TIME_FIELDS
    )


class ConditionInline(admin.TabularInline):
    model = ConditionModel


class ConditionalRedirectAdmin(admin.ModelAdmin):
    inlines = [ConditionInline]


class FormSubmitAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'submitter', 'export_to_pdf', 'download_attachments')
    list_filter = ('created',)
    actions = ['after_submit',]

    def export_to_pdf(self, obj):
        return '<a href="%s">%s</a>' % (obj.get_export_to_pdf_url(), _("Export"))
    export_to_pdf.allow_tags = True

    def download_attachments(self, obj):
        return '<a href="%s">%s</a>' % (obj.get_download_attachments_url(), _("Download"))
    download_attachments.allow_tags = True

    def after_submit(modeladmin, request, queryset):
        for submit in queryset.all():
            submit.after_submit()
    after_submit.short_description = "Run after submit actions"

class FormAdminExtend(FormAdmin):

    def get_queryset(self, req):
        queryset = super(FormAdminExtend, self).get_queryset(req)
        if req.user.is_superuser:
            return queryset
        forms = queryset.filter(permission=req.user)
        q = Q()
        forms_array = []
        for form in forms:
            forms_array += [form]
            forms_array += form.get_children()
        for a in forms_array:
            q |= Q(id=a.id)
        if q:
           return queryset.filter(q)
        return queryset.none()


class FormEntryAdminExtend(FormEntryAdmin):

    def get_queryset(self, req):
        queryset = super(FormEntryAdminExtend, self).get_queryset(req)
        if req.user.is_superuser:
            return queryset
        return queryset.filter(form__permission=req.user)


class FormSubmitAdminExtend(FormSubmitAdmin):

    def get_queryset(self, req):
        queryset = super(FormSubmitAdminExtend, self).get_queryset(req)
        if req.user.is_superuser:
            return queryset
        else:
            return queryset.filter(entries__submitter=req.user) | queryset.filter(entries__form__permission=req.user)



if not form_settings.FORK:
    admin.site.register(FieldModel, FieldAdmin)
    admin.site.register(FormModel, FormAdminExtend)
    admin.site.register(FormEntryModel, FormEntryAdminExtend)
    admin.site.register(FieldEntryModel, FieldEntryAdmin)
    admin.site.register(FormSubmitModel, FormSubmitAdminExtend)
    admin.site.register(ConditionalRedirectModel, ConditionalRedirectAdmin)
    admin.site.register(FormFieldModel, FormFieldAdmin)
