# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import re

from django.core.exceptions import ValidationError


def validate_regex_expr(value):
    try:
        re.compile(value)
    except Exception as e:
        raise ValidationError(e)
    return value
