# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views import generic
from django.conf import settings
from rest_form.models.entry import FormEntryModel
from django.db.models import Q
from rest_form.models.form import FormModel

from .mixins import FormURLMixin, BaseFormMixin, LoginRequiredMixin
from ..utils import get_submitter


class FormModelView(FormURLMixin,
                    BaseFormMixin,
                    generic.edit.ProcessFormView):
    template_name_field = 'page_template'
    context_object_name = 'form'
    login_url = '/admin/login'
    custom_next_url = None

    def get(self, request, *args, **kwargs):

        self.object = self.get_object(queryset=self.get_queryset())
        if self.object.is_formset:
            return HttpResponseRedirect(
                self.object.get_parent().get_absolute_url()
            )
        if not self.object.multiple and FormEntryModel.objects.filter(
            submitter=get_submitter(request),
            form=self.object,
            form_submit__isnull=False
        ).exists():
            return HttpResponseRedirect(
                reverse('rest_form:success', args=[self.object.get_root().url_path,])
            )

        return super(FormModelView, self).get(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=self.get_queryset())
        if self.object.get_root().login_required and not request.user.is_authenticated():
            return HttpResponseRedirect(self.get_login_url())
        response = super(FormModelView, self).dispatch(request, *args, **kwargs)
        return response

    def get_login_url(self):
        return getattr(settings, 'LOGIN_URL', self.login_url)

    def get_queryset(self):
        return FormModel.objects.get_for_request(self.request)

    def get_context_data(self, **kwargs):
        context = super(FormModelView, self).get_context_data(**kwargs)

        exclude_form_data = self.object.exclude_form_list(get_submitter(self.request), self.token)
        total_forms = self.get_forms_tree()
        q = Q()
        for exclude_form in exclude_form_data:
            q |= Q(id=exclude_form.id)
        total_forms = total_forms.exclude(q | Q(is_formset=True))
        context.update({
            'form_tree':total_forms,
            'progress': self.object.calculate_progress(self.request,total_forms, self.token),
            'user_entries': self.object.get_forms_entires(self.request, self.token)
        })
        return context

class SuccessURL(FormURLMixin,
                 generic.DetailView):
    template_name_field = 'success_page_template'

    def get_queryset(self):
        # TODO: permission check
        return FormModel.objects.active()
