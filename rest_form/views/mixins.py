# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid
import inspect
import datetime
import re

from django.db.models import Q
from django.http import JsonResponse, HttpResponseRedirect
from django.conf import settings
from django.contrib.auth.views import redirect_to_login, logout_then_login
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.core.urlresolvers import reverse
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import (HttpResponseRedirect, HttpResponsePermanentRedirect,
                         Http404, HttpResponse)
from django.shortcuts import get_object_or_404
from django.shortcuts import resolve_url
from django.utils.translation import ugettext as _
from django.utils.encoding import force_text
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _
from django.views import generic
from ..utils import get_submitter

# StreamingHttpResponse has been added in 1.5, and gets used for verification
# only.
try:
    from django.http import StreamingHttpResponse
except ImportError:
    class StreamingHttpResponse(object):
        pass

from rest_form.models.entry import FormEntryModel, FieldEntryModel
from rest_form.models.form import FormModel
from rest_form.forms import form_factory


class FormURLMixin(object):
    slug_field = 'url_path'
    slug_url_kwarg = 'path'


# https://github.com/brack3t/django-braces/blob/master/braces/views/_access.py#L88
class AccessMixin(object):
    """
    'Abstract' mixin that gives access mixins the same customizable
    functionality.
    """
    login_url = None
    raise_exception = False
    redirect_field_name = REDIRECT_FIELD_NAME  # Set by django.contrib.auth
    redirect_unauthenticated_users = False

    def get_login_url(self):
        """
        Override this method to customize the login_url.
        """
        login_url = self.login_url or settings.LOGIN_URL
        if not login_url:
            raise ImproperlyConfigured(
                'Define {0}.login_url or settings.LOGIN_URL or override '
                '{0}.get_login_url().'.format(self.__class__.__name__))

        return force_text(login_url)

    def get_redirect_field_name(self):
        """
        Override this method to customize the redirect_field_name.
        """
        if self.redirect_field_name is None:
            raise ImproperlyConfigured(
                '{0} is missing the '
                'redirect_field_name. Define {0}.redirect_field_name or '
                'override {0}.get_redirect_field_name().'.format(
                    self.__class__.__name__))
        return self.redirect_field_name

    def handle_no_permission(self, request):
        if self.raise_exception:
            if (self.redirect_unauthenticated_users
                    and not self.request.user.is_authenticated()):
                return self.no_permissions_fail(request)
            else:
                if (inspect.isclass(self.raise_exception)
                        and issubclass(self.raise_exception, Exception)):
                    raise self.raise_exception
                if callable(self.raise_exception):
                    ret = self.raise_exception(request)
                    if isinstance(ret, (HttpResponse, StreamingHttpResponse)):
                        return ret
                raise PermissionDenied

        return self.no_permissions_fail(request)

    def no_permissions_fail(self, request=None):
        """
        Called when the user has no permissions and no exception was raised.
        This should only return a valid HTTP response.
        By default we redirect to login.
        """
        return redirect_to_login(request.get_full_path(),
                                 self.get_login_url(),
                                 self.get_redirect_field_name())


class LoginRequiredMixin(AccessMixin):
    """
    View mixin which verifies that the user is authenticated.
    NOTE:
        This should be the left-most mixin of a view, except when
        combined with CsrfExemptMixin - which in that case should
        be the left-most mixin.
    """
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return self.handle_no_permission(request)

        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class AjaxResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    NOTIFY_INFO, NOTIFY_ERROR,  NOTIFY_WARNING, NOTIFY_SUCCESS = \
        'info', 'error', 'warn', 'success'

    def get_json_payload(self, redirect=False, notify=None,  errors=None,
                         data=None):
        redirect_url = ""
        if redirect:
            redirect_url = self.get_success_url()

        return {
            'notify': notify,
            'action': {
                'redirect': redirect,
                'redirect_url': redirect_url
            },
            'errors': errors,
            'data': data
        }

    def form_invalid(self, form):
        response = super(AjaxResponseMixin, self).form_invalid(form)
        notify = []
        payload = {'errors': []}
        if any(form.errors):
            payload.update(errors=form.errors)
            notify.append({
                'message': _('Please correct form errors marked in red'),
                'type': self.NOTIFY_ERROR
            })

        # if self.object.is_formset:
        #     for error in form.non_form_errors():
        #         notify.append({
        #             'message': error,
        #             'type': self.NOTIFY_ERROR
        #         })
        # else:
        for error in form.non_field_errors():
            notify.append({
                'message': error,
                'type': self.NOTIFY_ERROR
            })
        payload.update(notify=notify)

        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(**payload), status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super(AjaxResponseMixin, self).form_valid(form)
        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(redirect=True))
        else:
            return response


class FormModelLoginRequiredMixin(object):
    """
    This view must check if FormModel object is required or not.
    """
    pass


class BaseFormMixin(AjaxResponseMixin,
                    generic.edit.FormMixin,
                    generic.detail.SingleObjectMixin,
                    generic.detail.SingleObjectTemplateResponseMixin):
    """
    Mixin to render FormModel in template and validate.
    """

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.form_entry = FormEntryModel.objects.filter(
            submitter=get_submitter(request),
            token=self.token,
            form=self.object
        )
        self.update = self.form_entry.exists()
        self.custom_next_url = self.request.GET.get('next-url', None)
        return super(BaseFormMixin, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if self.update:
        #     self.form_entry.first().field_entries.files().delete()
        return super(BaseFormMixin, self).get(request, *args, **kwargs)

    def process_file(self, request, form_model):
        formset_id = None
        formset_order = None
        if '-' in request.FILES.keys()[0]:
            # formset field posted, find the field on that form
            # formset-[form_id]-[order]-[field_name]
            field_opts = request.FILES.keys()[0].split('-')
            original_file_name = request.FILES.keys()[0]
            formset_order = field_opts[2]
            formset_id = field_opts[1]
            field_name = field_opts[3]
            form_model = FormModel.objects.get(pk=int(formset_id))
            fields = form_model.get_fields().files().filter(
                field__name=field_name
            )
            if fields.count() != 1:
                return -1

            field = fields[0].field

            FieldEntryModel.objects.filter(
                form=self.object,
                field_name=field.name,
                formset_order=formset_order,
                formset_id=formset_id,
                field=field,
                submitter=get_submitter(request),
                token=self.token,
                file__isnull=False
            ).delete()

            field_entry = FieldEntryModel.objects.create(
                form=self.object,
                field_name=field.name,
                formset_order=formset_order,
                formset_id=formset_id,
                field=field,
                file=request.FILES.get(original_file_name),
                submitter=get_submitter(request),
                token=self.token
            )

        else:
            # field posted, find in current form's fields
            field_name = request.FILES.keys()[0]
            fields = form_model.get_fields().files().filter(
                field__name=field_name
            )
            if fields.count() != 1:
                return -1

            field = fields[0].field

            # remove the old file if we upload another one over the same formset_id
            FieldEntryModel.objects.filter(
                form=self.object,
                field_name=field.name,
                formset_order=formset_order,
                formset_id=formset_id,
                field=field,
                submitter=get_submitter(request),
                token=self.token,
                file__isnull=False
            ).delete()

            field_entry = FieldEntryModel.objects.create(
                form=self.object,
                field_name=field.name,
                formset_order=formset_order,
                formset_id=formset_id,
                field=field,
                file=request.FILES.get(field.name),
                submitter=get_submitter(request),
                token=self.token
            )

        return field_entry.pk

    def post(self, request, *args, **kwargs):
        """
        We need the object attribute here for form_factory to create the form.
        """
        if request.FILES:
            pk = self.process_file(request, self.object)
            return JsonResponse({'pk': pk})
        elif request.POST.get('file_delete'):
            response = dict()
            pk = request.POST.get('file_delete')
            try:
                FieldEntryModel.objects.get(pk=pk).delete()
            except FieldEntryModel.DoesNotExist:
                # Field doesn't exist in database while file uploading
                # so pk assign as -1
                # while deleting it post pk  wtih -1 & no record found for -1 pk
                response['errors'] = "File not exist"
            except:
                response['errors'] = "Unknown error"
            return JsonResponse(response)
        else:
            if self.object.is_formset:
                formset = self.get_formset_class(
                                form=self.get_form_class(),
                                form_model=self.object,
                                token=self.token,
                                submitter=get_submitter(request)
                                    )(request.POST, request.FILES, prefix=self.object.formset_prefix)
                if formset.is_valid():
                    for index, form in enumerate(formset):
                        if form.is_valid():
                            files = request.POST.get('formset-%s-%s-_files' % (self.object.pk, index))
                            entry = form.save(files=files)
                            if self.request.session.get('form_pks'):
                                request.session['form_pks'].append(entry.id)
                            else:
                                request.session['form_pks'] = [entry.id]
                        else:
                            return self.form_invalid(form)
                    return self.formset_valid(formset)
                else:
                    return self.formset_invalid(formset)  
            elif self.object.has_formset_childrens():
                form = self.get_form()
                if form.is_valid():
                    valid, response = self.validate_files(form)
                    if not valid:
                        return response
                    entry = form.save(files=self.request.POST.get('_files', None), update=self.update)
                    if self.request.session.get('form_pks'):
                        self.request.session['form_pks'].append(entry.id)
                    else:
                        self.request.session['form_pks'] = [entry.id]
                    for i, form_model in enumerate(self.object.get_children()):
                        if form_model.is_formset:
                            form = form_factory(
                                form_model,
                                token=self.token,
                                request=self.request,
                                is_formset=True,
                            )
                            formset = self.get_formset_class(form=form, form_model=form_model,
                                                             token=self.token, submitter=get_submitter(request))
                            formset = formset(request.POST, prefix='formset-%s' % form_model.pk)
                            if formset.is_valid():
                                for index, form in enumerate(formset):
                                    if form.is_valid():
                                        valid, response = self.validate_files(form, formset)
                                        if not valid:
                                            return response
                                        files = request.POST.get('formset-%s-%s-_files' % (form_model.pk, index))
                                        entry = form.save(parent=entry, files=files, update=self.update)
                                        if self.request.session.get('form_pks'):
                                            request.session['form_pks'].append(entry.id)
                                        else:
                                            request.session['form_pks'] = [entry.id]
                                    else:
                                        return self.form_invalid(form)
                            else:
                                return self.formset_invalid(formset)  
                    return super(BaseFormMixin, self).form_valid(entry)
                else:
                    return self.form_invalid(form)
                # return super(BaseFormMixin, self).post(request, *args, **kwargs)
            else:          
                return super(BaseFormMixin, self).post(request, *args, **kwargs)

    @staticmethod
    def get_formset_class(form, form_model, token, submitter, formset_class=BaseFormSet, extra=1):
        """
        Create a formset from current form, using attributes from FormModel
        obj.
        """
        if form_model.formset_min_num > 0:
            extra = 0
        return formset_factory(
            form,
            formset_class,
            extra=extra,
            can_order=form_model.formset_can_order,
            can_delete=form_model.formset_can_delete,
            max_num=form_model.formset_max_num,
            min_num=form_model.formset_min_num,
            validate_max=True,
            validate_min=True,

        )

    def get_form_class(self):
        """
        Factory the form on the fly from the current object. If form is defined
        as formset return None.
        """
        return form_factory(
            self.object,
            token=self.token,
            request=self.request,
            update=self.update,
            is_formset=self.object.is_formset
        )

    def get_form(self, form_class=None):
        if self.object.is_formset:
            form = self.get_formset_class(
                form=self.get_form_class(),
                form_model=self.object,
                token=self.token,
                submitter=get_submitter(self.request)
            )
            kwargs = self.get_form_kwargs()
            kwargs.update(prefix='formset-%s'% (self.object.pk))
            return form(**kwargs)
        else:
            form = self.get_form_class()
            return form(**self.get_form_kwargs())

    def get_success_url(self):
        # TODO: Make this a customizable success_url on FormModel.
        if self.request.session.get('form_pks'):
            self.request.session['entry_pks'] =  self.request.session.get('form_pks')
        entry_pks = self.request.session.get('entry_pks')
        if self.object.is_last(get_submitter(self.request), self.token):
            try:
                self.request.session.pop('form_pks')
            except Exception as e:
                print e

        return self.object.get_redirect_url(self.request, get_submitter(self.request), self.token, entry_pks, self.custom_next_url)

    def form_valid(self, form):
        valid, response = self.validate_files(form)
        if not valid:
            return response
        entry = form.save(files=self.request.POST.get('_files', None), update=self.update)
        if self.request.session.get('form_pks'):
            self.request.session['form_pks'].append(entry.id)
        else:
            self.request.session['form_pks'] = [entry.id]
        return super(BaseFormMixin, self).form_valid(entry)

    def validate_files(self, form, formset=None):
        formset_id = None
        formset_order = None
        if formset:
            fs, formset_id, formset_order = form.prefix.split('-')
        errors = {}
        for file_field in self.object.get_fields().files():
            if file_field.field.required:
                if not FieldEntryModel.objects.filter(
                    form=self.object,
                    field=file_field.field,
                    field_name=file_field.field.name,
                    submitter=get_submitter(self.request),
                    token=self.token,
                    formset_id=formset_id,
                    formset_order=formset_order
                ).exists():
                    prefix = form.prefix + '-' if form.prefix else ''
                    field_name = '{}{}'.format(prefix, file_field.field.name)
                    errors.update({field_name: [_('This field is required'),]})
        if errors:
            notify = [{
                'message': _('Please correct form errors marked in red'),
                'type': self.NOTIFY_ERROR
            }]
            return False, JsonResponse(self.get_json_payload(
                errors=errors, notify=notify), status=400)
        return True, None

    def formset_invalid(self, formset):
        notify = []
        payload = {'errors': []}
        for index, form in enumerate(formset):
            for error in form.errors:

                error_data = {
                    '%s-%s-%s' %(formset.prefix, index, error): form.errors[error]
                }
                payload['errors'].append(error_data)
        notify.append({
            'message': _('Please correct form errors marked in red'),
            'type': self.NOTIFY_ERROR
        })
        for error in formset.non_form_errors():
            notify.append({
                'message': error,
                'type': self.NOTIFY_ERROR
            })
        payload.update(notify=notify)

        # if self.request.is_ajax():
        return JsonResponse(self.get_json_payload(**payload), status=400)
        # else:
        #     return self.render_to_response(self.get_context_data(form=form))

    def formset_valid(self, formset):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        if self.request.is_ajax():
            return JsonResponse(self.get_json_payload(redirect=True))
        else:
            return HttpResponseRedirect(self.get_success_url())

    def get_forms_tree(self):
        return self.object.get_tree()

    def get_context_data(self, **kwargs):
        kwargs = super(BaseFormMixin, self).get_context_data(**kwargs)

        formsets = []
        if self.object.has_formset_childrens():
            for i, form_model in enumerate(self.object.get_children()):
                    if form_model.is_formset:
                        form = form_factory(
                            form_model,
                            token=self.token,
                            request=self.request,
                            update=self.update,
                            is_formset=True
                        )
                        initial = self.get_initial_for_formset(form_model)
                        extra = 0 if initial else 1

                        formset = self.get_formset_class(form=form, form_model=form_model, extra=extra,
                                                         token=self.token, submitter=get_submitter(self.request))
                        formset = formset(prefix='formset-%s'% (form_model.pk,), initial=initial)
                        formsets.append(formset)

            kwargs['children_formsets'] = formsets
        kwargs['is_final'] = self.object.is_last(get_submitter(self.request), self.token)
        return kwargs

    def get_initial_for_formset(self, form_model):
        initial = []
        form_entries = FormEntryModel.objects.filter(
            # parent__form=form_model,
            token=self.token,
            submitter=get_submitter(self.request),
            form__is_formset=True
        )
        for form_entry in form_entries:
            form_entry_dict = {}
            for field_entry in form_entry.field_entries.all():
                form_entry_dict.update({
                    field_entry.field.name: field_entry.value})
            if form_entry_dict:
                initial.append(form_entry_dict)
        return initial

    @cached_property
    def token(self):
        form_model = self.get_object()
        session_key = '{}-token'.format(form_model.name)
        is_root = form_model.is_root()
        if is_root and session_key in self.request.session:
            return uuid.UUID(self.request.session[session_key])
        else:
            form_entry = FormEntryModel.objects.filter(
                submitter=get_submitter(self.request),
                form=form_model.get_root(),
                form_submit__isnull=True,
            )
            if form_entry.count() == 0:
                # first entry
                token = uuid.uuid4()
            else:
                if form_entry.first().form.multiple:
                    # form can have multiple entries, return new token uuid
                    token = uuid.uuid4()
                # else, if we have entry for the form then return that token
                token = form_entry.first().token
            if is_root:
                self.request.session[session_key] = str(token)
            return token
