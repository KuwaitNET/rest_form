# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict

from django import forms
from django.db import transaction
from django.forms.widgets import RadioChoiceInput, RadioFieldRenderer
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from rest_form.models.entry import FormEntryModel, FieldEntryModel
from rest_form.utils import get_submitter


class RestFormClass(forms.Form):
    """
    Used to instantiate a RestFormClass from rest_form.models.form.FormModel
    """

    def __init__(self, *arg, **kwarg):
        super(RestFormClass, self).__init__(*arg, **kwarg)
        self.empty_permitted = False

    def save_field_entries(self, form_entry):
        form_fields = self.form_model.get_fields().non_files()
        form_fields = form_fields.filter(
            field__name__in=self.cleaned_data.keys()
        )

        field_entries = [
            FieldEntryModel(
                form=form_entry.form,
                entry=form_entry,
                field=form_field.field,
                field_name=form_field.field.name,
                value=self.cleaned_data.get(form_field.field.name),
                submitter=get_submitter(self.request),
                token=form_entry.token
            ) for form_field in form_fields
        ]

        FieldEntryModel.objects.bulk_create(field_entries)

    def save_file_entries(self, form_entry, files):
        if files:
            file_field_ids = self.form_model.get_fields().files().values_list('field__pk', flat=True)
            file_entries = FieldEntryModel.objects.filter(
                id__in=[int(file) for file in files.split(',')],
            ).update(entry=form_entry)

    @transaction.atomic
    def save(self, parent=None, files=None, update=False):
        submitter = get_submitter(self.request)
        # if update and parent:
        #     form_entry = parent
        #     form_entry.request = self.request
        # elif update and not parent:

        if update and not parent:
            form_entry = FormEntryModel.objects.get(
                submitter=submitter,
                token=self.token,
                form=self.form_model,
            )
            form_entry.request = self.request
            # Removes old field entries
            form_entry.field_entries.fields().delete()
            # FormEntryModel.objects.filter(
            #     parent=form_entry,
            #     submitter=submitter,
            #     token=self.token,
            # ).delete()
        else:
            form_entry = FormEntryModel.objects.create_for_form(
                form=self.form_model,
                submitter=submitter,
                token=self.token,
                request=self.request,

            )
        if parent:
            form_entry.parent = parent
            form_entry.save()
        else:
            if self.form_model.is_root():
                form_entry.parent = form_entry
                form_entry.save()
            else:
                try:
                    form_entry.parent = FormEntryModel.objects.filter(submitter=submitter,
                                         token=self.token, form=self.form_model.get_root()).first()
                    form_entry.save()
                except Exception as e:
                    print e

        self.save_field_entries(form_entry)
        self.save_file_entries(form_entry, files)

        return form_entry


class FileUploadForm(forms.ModelForm):

    class Meta:
        model = FieldEntryModel
        fields = ('field_name',  'field', 'file')

    def clean_field(self):
        entry = self.cleaned_data.get('entry')
        files = entry.form.get_fields().files()
        return files.filter(field__name=self.cleaned_data.get('field_name')).first()


def form_factory(form_model, token, request, update=False, is_formset=False,
                 form_class=RestFormClass):
    """
    Creates a django.forms.Form class from rest_form.models.form.FormModel
    instance.
    """

    def _get_declared_fields(fields):
        fields = [
            (form_field.field.name, form_field.field.instantiate_form(
                token, request, update, is_formset, form_model)) for form_field in fields
            ]
        fields.sort(key=lambda x: x[1].creation_counter)
        return OrderedDict(fields)

    base = type(str('Base'), (object,), {
        'base_fields': _get_declared_fields(form_model.get_fields()),
        'declared_fields': _get_declared_fields(form_model.get_fields()),
        'form_model': form_model,
        'token': token,
        'request': request
    })

    form_class = type(
        str('%sForm' % form_model.slug),  # TODO: Update class name
        (base, form_class),
        {}
    )

    return form_class


class RadioChoiceInputWithClass(RadioChoiceInput):
    
    def __init__(self, name, value, attrs, choice, index):
        trigger = choice[2]
        if 'class' in attrs and trigger:
            attrs['class'] += ' other-choice-trigger'
        elif trigger:
            attrs['class'] = 'other-choice-trigger'
        super(RadioChoiceInputWithClass, self).__init__(name, value, attrs, choice, index)


class RadioFieldRendererWithClass(RadioFieldRenderer):
    choice_input_class = RadioChoiceInputWithClass


    def render(self):
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        """
        id_ = self.attrs.get('id')
        output = []
        for i, choice in enumerate(self.choices):
            choice_value, choice_label = choice[0], choice[1]
            if isinstance(choice_label, (tuple, list)):
                attrs_plus = self.attrs.copy()
                if id_:
                    attrs_plus['id'] += '_{}'.format(i)
                sub_ul_renderer = self.__class__(
                    name=self.name,
                    value=self.value,
                    attrs=attrs_plus,
                    choices=choice_label,
                )
                sub_ul_renderer.choice_input_class = self.choice_input_class
                output.append(format_html(self.inner_html, choice_value=choice_value,
                                          sub_widgets=sub_ul_renderer.render()))
            else:
                w = self.choice_input_class(self.name, self.value,
                                            self.attrs.copy(), choice, i)
                output.append(format_html(self.inner_html,
                                          choice_value=force_text(w), sub_widgets=''))
        return format_html(self.outer_html,
                           id_attr=format_html(' id="{}"', id_) if id_ else '',
                           content=mark_safe('\n'.join(output)))


class ChoiceWithOtherWidget(forms.MultiWidget):
    """MultiWidget for use with ChoiceWithOtherField."""
    def __init__(self, choices):
        widgets = [
            forms.RadioSelect(
                renderer=RadioFieldRendererWithClass,
                choices=choices,
                attrs={'class': 'other-choice-button'}
            ),
            forms.TextInput(attrs={'class': 'inputbox other-choice-text', 'style': 'display: none;'})
        ]
        super(ChoiceWithOtherWidget, self).__init__(widgets)

    def decompress(self, value):
        if not value:
            return [None, None]
        return value

    def format_output(self, rendered_widgets):
        return rendered_widgets[0] + '<br>' + rendered_widgets[1]


class ChoiceWithOtherField(forms.MultiValueField):
    """
    ChoiceField with an option for a user-submitted "other" value.
    """
    def __init__(self, *args, **kwargs):
        self.triggers = []
        choices_before = kwargs['choices']
        choices_after = []
        for choice in choices_before:
            if choice[2]:
                self.triggers.append(str(choice[0]))
            choices_after.append((choice[0], choice[1]))
        kwargs['choices'] = choices_after
        fields = [
            forms.ChoiceField(widget=forms.RadioSelect(), *args, **kwargs),
            forms.CharField(required=False),
        ]
        widget = ChoiceWithOtherWidget(choices=choices_before)
        kwargs.pop('choices')
        self._was_required = kwargs.pop('required', True)
        kwargs['required'] = False
        super(ChoiceWithOtherField, self).__init__(widget=widget, fields=fields, *args, **kwargs)

    def compress(self, value):
        if not value:
            return u''
        if self._was_required and not value or value[0] in (None, '') or (value[0] in self.triggers and not value[1]):
            raise forms.ValidationError(self.error_messages['required'])
        if all(value):
            return '; '.join(value)
        else:
            return value[0]