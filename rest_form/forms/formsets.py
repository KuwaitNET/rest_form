# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.forms.formsets import BaseFormSet


class RestBaseFormSet(BaseFormSet):

    def __init__(self, *args, **kwargs):
        super(RestBaseFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def save(self):
        pass