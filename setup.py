#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from setuptools import setup, find_packages

import rest_form


def convert(filename):
    if sys.version_info[0] < 3:
        with open(filename) as fd:
            return fd.read()
    else:
        with open(filename, encoding="utf-8") as fd:
            return fd.read()


CLASSIFIERS = [
    'Environment :: Web Environment',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License (GPL)'
    'Development Status :: 2 - Pre-Alpha',
    'Operating System :: OS Independent',
    'Topic :: Software Development :: Libraries',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content'
    'Programming Language :: Python',
]

setup(
    author='Dacian Popute',
    author_email='dacian@kuwaitnet.com',
    name='rest_form',
    version=rest_form.__version__,
    description='Dynamic django forms through REST API.',
    platforms=['OS Independent'],
    classifiers=CLASSIFIERS,
    long_description=convert('README.rst'),
    url='https://bitbucket.org/KuwaitNET/rest_form',
    packages=find_packages(exclude=['example', 'docs', 'tests*']),
    install_requires=[
        'django>=1.8,<=1.9.9',
        'djangorestframework==3.4.7',  # Important due to emtpy request.data https://github.com/tomchristie/django-rest-framework/issues/3951
        'django-treebeard==4.0.1',
        'django-simple-captcha==0.5.2',
        'django-ipware==1.1.5',
        'jsonfield==1.0.3',
        'six>=1.10.0,<1.20',
        'django-admin-sortable==2.0.19',
        'django-autoslug==1.9.3',
        'django-ckeditor==5.1.1'
    ]
)
